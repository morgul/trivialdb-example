//----------------------------------------------------------------------------------------------------------------------
// model.js - A file to hold out database instances. We use the term 'model' here, but 'store' is more accurate.
//
// @module
//----------------------------------------------------------------------------------------------------------------------

const tdb = require('trivialdb');

//----------------------------------------------------------------------------------------------------------------------
// Models
//
// We declare all of the specific database instances we want to create. Each one will be backed by a json file in
// `server/db`, as we have it configured. You can play with passing in other options too, check the TrivialDB
// documentation for details.
//
//----------------------------------------------------------------------------------------------------------------------

// This represents a user account, and will live in `server/db/accounts.json`.
const Account = tdb.db('accounts', { dbPath: 'server/db' });

// This represents a blog post by a user, and will live in  `server/db/posts.json`.
const Post = tdb.db('posts', { dbPath: 'server/db' });

//----------------------------------------------------------------------------------------------------------------------

module.exports = {
    Account,
    Post,

    // We expose trivialdb's errors object here so we can easily catch known errors, but avoid importing trivialdb in
    // ever module that uses our models.
    errors: tdb.errors
}; // end exports

//----------------------------------------------------------------------------------------------------------------------
