//----------------------------------------------------------------------------------------------------------------------
// Example TrivialDB Powered Application
//
// @module
//----------------------------------------------------------------------------------------------------------------------

const _ = require('lodash');
const Promise = require('bluebird');
const { Account, Post } = require('./server/models');

//----------------------------------------------------------------------------------------------------------------------

// TrivialDB has to load files from the disk, so we have to wait for it to be ready. Each database has it's own loading
// promise, which thankfully is pretty easy to wait on:

Promise.join(
        Account.loading,
        Post.loading
    )
    .then(() =>
    {
        // Inform the user we've finished loading.
        console.log('Finished loading both databases. Running script...\n');
    })
    .then(() =>
    {
        // Let's start with a very basic question: How many admins and regular users do we have in the system?

        const admins = Account.query()
            .filter({ admin: true })
            .run();

        const regulars = Account.query()
            .filter((account) => !account.admin) // Since `admin: null` should also count, we use a filter function.
            .run();

        // And now we display
        console.log(`We have ${ admins.length } admins, and ${ regulars.length } users.\n`);
    })
    .then(() =>
    {
        // Let's add an Account.

        // First create the object we want to save in the database
        const newAccount = {
            firstname: "New",
            lastname: "User",
            username: "nuser",
            age: 25,
            settings: { displayEmail: false },
            email: "nuser@example.com"
        };

        return Account.save(newAccount)
            .then((accountID) =>
            {
                // We get back the new account id, so we can pull it later:
                console.log(`New account id: ${ accountID }`);

                // If you don't believe me, watch:
                const account = Account.get(accountID);

                // And his name is 'New User':
                console.log(`Got back user: ${ account.firstname } ${ account.lastname }.\n`);

                // Hand this useful bit of info off to the next step.
                return accountID;
            })
            .then((accountID) =>
            {
                // We should probably clean up after ourselves. So let's delete the user we made.

                // We don't remove by id; rather we filter the database, and remove any who match.
                return Account.remove({ id: accountID })
                    .then(() =>
                    {
                        // Let's make sure that he's gone.
                        const account = Account.get(accountID);

                        if(account)
                        {
                            console.error('Oh, no! We failed to delete the user!!!');
                        }
                        else
                        {
                            console.log('Hurray! The user was deleted!!\n');
                        } // end if
                    });
            });
    })
    .then(() =>
    {
        // Now it's time to play with Posts. Since queries are just lodash strings, let's print out all the titles of
        // the posts, by author, sorted bu oldest to newest.

        const postStrings = Post.query()
            .sortBy('created')
            .map((post) =>
            {
                return `Title: "${ post.title }", author: ${ post.user }`;
            })
            .run();

        console.log('All posts:');

        _.each(postStrings, (string) => console.log('  ' + string));

        // Add a blank line
        console.log('');
    })
    .then(() =>
    {
        // Ok, cool. Now let's use multiple databases!

        // Get the username with the most posts (this is some advanced lodash magic, don't worry about it)
        const user = Post.query()
            .reduce((results, post) =>
            {
                const user = post.user;

                results[user] = (_.get(results, user, 0)) + 1;

                return results;
            }, {})
            .toPairs()
            .sortBy('1')
            .last()
            .run()[0];

        // Now, let's get the account for this username:
        const account = Account.query().filter({ username: user }).first().run();

        // And the winner is?
        console.log(`The user with the most posts is: ${ account.firstname } ${ account.lastname }.\n`)
    })
    .then(() =>
    {
        // And finally, let's join some data.

        const posts = Post.query()
            .map((post) =>
            {
                // We add an account property to post, by looking it up in the Account database
                post.account = Account.query().filter({ username: post.user }).first().run();
                return post;
            })
            .run();

        console.log('All posts, with full author name:');

        _.each(posts, (post) =>
        {
            console.log(`  Title: ${ post.title }, author: ${ post.account.firstname } ${ post.account.lastname }`);
        });
    });


//----------------------------------------------------------------------------------------------------------------------
