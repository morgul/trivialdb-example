# TrivialDB Examples

This project is setup like a typical server using TrivialDB woul be, except there's no actual server, just an `app.js`
script that gives examples. (_Also, the data files are committed, which I **do not** recommend; back them up like you
would any other database, and leave the files out of the repo._)

The examples are an attempt to show near-real world use cases for TrivialDB, and give you a feel for working with it.

As always, the documentation is the best guide for how to do something specific.

## Usage

```bash
$ node ./app.js
```

That's it!

## Understanding the Code

You will want to look in `/server/models.js` for defining a database, `/server/db` for the json files saved to disk, and
`/app.js` for examples querying, saving, deleting, and joining.

## Editing Data

You can edit any data you want, by directly editing the `.json` files in `/server/db`. Feel free to play with them and
watch the output change.

_Note: TrivialDB does not detect changes to the data. If you wish, you can reload the database by calling `reload()` on
any database instance._
